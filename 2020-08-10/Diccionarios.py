#definicion de un diccionario
diccionario = {
    #Elemento : [Valor]
    "Perro": "Chispita",
    "Tortuga": "Veloz",
    "Gato": "curioso",
#Lo que hago es crear elementos y definir el valor de esos elementos, no se limita solo a cadenas, puedo tener enteros u otro tipo de variables

}

#Elemento Perro
print(diccionario["Perro"]) #en vez de colocar el indice coloco el denominador del elemento
#Al diccionario le puedo agregar otro diccionario y lo llamo como matriz [dicionario2][elementodeldiccionario]
#tambien listas y puedo seleccionar el elemento de las listas como los llamo comunmente 
#ejemplo diccionario[lista][0]

