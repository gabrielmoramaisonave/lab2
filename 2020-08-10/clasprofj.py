#con funcion type(variable), me dice que tipo de variable es.
cadena="Hola"
cadena2="Mundo!"

print(type(cadena))

#concatenacion de variables
print(cadena+" "+cadena2) #el operador de concatenacion es el +

#cadena caso especial
cadena3= '-Gabriel: "Hola!"' #para que se muestre comillas dobles en pantalla le meto comillas simple y viceversa
print(cadena3)

#conversion de tipos para concatenar o usar operadores matematicos

cadenaNumero= "56"
print(int(cadenaNumero)+1)
print("Este es el numero "+str(cadenaNumero))

