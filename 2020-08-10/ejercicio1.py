#variables
numero_entero = 10
numero_decimal = 10.4
cadena = "Un texto"
boolean = True #True o False, importante la primer letra en mayuscula, sino no lo toma. 

# comentarios
print("Hola mundo!")
print(cadena , numero_entero , numero_decimal, boolean)
print(numero_decimal)
print(cadena)

#forma de escribir similar al printf en C

# ejemplo en c printf("El numero vale %d", numero_entero).
# de esta forma acomodamos como en C, lo que vaya despues de format en ese orden va a salir entre las llaves
print("El numero vale {0}".format(numero_entero))
#ej 2
print("El numero vale {0} {1}".format(numero_entero, numero_decimal)) #puedo repetir el parametro para que lo imprima más veces, ej {0} {1} {0}, me imprime 10 10.4 10
#otra opcion
print(f"El numero vale {numero_entero}") #se coloca entre llaves el nombre de variable, pero antes de las comillas colocamos una f para decirle q esta formateado, seria una forma simplificado
