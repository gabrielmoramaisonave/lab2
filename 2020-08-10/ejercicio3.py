nombre= input("Ingrese nombre: ") #lee lo que colocamos en este caso lo guarda en nombre
#en input se guarda una cadena de caracteres, si ingresamos 10, lo toma como cadena de caracteres, no como entero
print(nombre)
#para que los guarde como numeros usamos esta funcion. Int convierte cadena de caracteres en entero
num1 = int(input("Ingrese numero 1: "))
num2 = int(input("Ingrese numero 2: "))
#funcion float los transforma en flotantes
print(num1 + num2)
#otra forma de hacerlo
#guardamos en input normal y transformamos a entero o flotante al usarlo:

# num1 = input("Ingrese numero 1: ")
# num2 = input("Ingrese numero 2: ") 
# print(int(num1)) + int(num2)


