#cadena de caracteres

#formas de escribir con comilla simple y doble. 
cadena1 = 'Una "cadena" de texto'#nos sirven comillas simples o doble segun que querramos meter una en el texto
cadena2 = "Una \"cadena\" de texto"#con el caracter de escape \ podemos imprimir comillas doble aun usando las dobles
#\t sirve para tabulacion
#\n sirve para salto de linea, como en c

print(cadena1) 
print(cadena2)

#cadena de varias lineas con '''
cadena3 = ''' 
una cadena
de varias 
lineas '''
print(cadena3)

