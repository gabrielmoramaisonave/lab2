#tipos de funciones con cadenas
#funcion type para saber el tipo
cadena = "texto"
print(type(cadena))
#funcion dir me indica que puedo hacer con ese tipo de dato, metodos, funciones que forman parte del objeto. En primera instancia usamos las que no tienen doble __

cadena = "texto"
print(dir(cadena))

#ejemplo funcion upper que sacamos de dir (sirve para que pase todo a mayuscula)
cadena = "texto"
print(cadena.upper())
#las funciones que empiesan es is preguntan algo, o sea que la respuesta es True o False
#la funcion istitle es para saber si las palabras empiezan con mayuscula

#funcion replace : sirve para cambiar algo en este caso es en minuscula por ES

cadena = "Esto es un texto"
print(cadena.replace("es", "ES"))

#funcion index : me busca el caracter y me dice donde lo encuentro por primera vez

cadena = "Esto es un texto"
print(cadena.index("es"))

#funcion find : tambien para buscar. dif con index, me retorna otros valores como -1 sino lo encuentra. Index tira error.

#Llamar cadena como arreglo. Podemos llamar un caracter como si fuese un arreglo
#si pongo indice -1 me sale la última letra. Empieza a contar desde el último si el indice es negativo
cadena = "Esto es un texto"
print(cadena[5])

#funcion count : cuenta cuantas veces hay un caracter o texto en la cadena

cadena = "Esto es un texto"
print(cadena.count("e"))

#con len me cuenta la cantidad de caracteres de la cadena
cadena = "Esto es un texto"
print(len(cadena))
