#las listas pueden tener distintos tipos de variables a diferencia de los arrays en c
# la lista se define asi:
#Nombre de lista = [parametros]
lista = [0, 5, 2, 5]
print(lista) #me imprime con corchetes porque me imprime lista completa

#podemos llamar por indice como en los arrays
print(lista[3])
print(lista[-2])

#se pueden concatenar las listas


#Operador :

#de esta forma (con :) me imprime lo que esta desde el primer parametro hasta el segundo, pero el segundo no lo toma)
#es decir en este caso toma el elemento 1 e imprime hasta el 2, el 3 no lo incluye

print(lista[1:3])
print(lista[:3])#si no coloco nada toma desde el principio, si le quito el ultimo me lo toma hasta el final de la lista
print(lista[0:-2]) #si pongo indice negativo toma desde la derecha a la izquierda
print(lista[-3:-1]) #siempre del menor al mayo, el -1 negativo es el ultimo.

#SIEMPRE EL INDICE QUE VA DESPUES DEL : ES UNO MENOS AL QUE INDICA PORQUE NO LO INCLUYE

