lista = [0, 5, 2, 5, -1, 10, 3, 2]

#len para saber los caracteres
print(lista)
print(len(lista))

#funcion append para agregar otros elementos o lista.

lista.append(45)
print(lista)

#funcion insert sirve para agregar pero indicando la posicion

lista.insert(2, 6) #primer elemento es la posicion despues de la coma lo que agrego
print(lista)

#funcion sort :sirve para ordenar los elementos de una lista de manera ascendente.

lista.sort() #si coloco lista.sort(reverse=True) lo ordena de mayor a menor
print(lista)

#funcion reverse sirve para escribir la lista al reves

lista.reverse()
print(lista)

#quita por indice
#funcion pop : sirve para eliminar un elemento de la lista. le indico el elemento con el indice


lista.pop(3)
print(lista)

#Quita por elemento, y quita el primero que encuentra, no todos
#funcion remove tambien quita pero tengo que especificarle que quiero quitar el nombre del elemento
#ej numero 45

lista.remove(45)
print(lista)

#funcion extend para agregar al final de la lista nuevos elementos
#tiene que ser una lista lo que extienda

lista.extend([-3, -7])
print(lista)

#operador in
#me permite preguntar si existe un elemento dentro de la lista o cadena

print(5 in lista)