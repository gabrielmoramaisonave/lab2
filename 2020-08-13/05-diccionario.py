dic = {
    "banana": "amarilla",
    "manzana": "roja",
    "pera": "verde"
}

print(dic)

print(dic["banana"]) #me devuelve solo amarilla en este caso

#funcion para que me devuelva todos los elementos agrupados con su significante

print(dic.items())

#funcion dic.values() me devuelve la lista con los significantes

print(dic.values())

#funcion Keys, me da lista de las llaves

print(dic.keys()) #me devuelve lista de llaves, es decir los elementos a los cuales le damos los valores o insignificantes